clear; clc;

im = imread('webstershapes.png');
[H,S,V] = rgb2hsv(im);
NUM_DETECTED = 100;
lab = uint8(rgb2lab(im));

[m,n,d] = size(im);

f = lab(:,:,1);
f = rgb2gray(im);

SURFpoints = detectSURFFeatures(f);
BRISKpoints = detectBRISKFeatures(f);

SURFstrongest = SURFpoints.selectStrongest(NUM_DETECTED);
BRISKstrongest = BRISKpoints.selectStrongest(NUM_DETECTED);

d = zeros(m,n);
l = round(SURFstrongest.Location);
SURFstrongest.Location = l;

[numPointsOnImage,~] = size(SURFstrongest.Location);

for point = 1:numPointsOnImage
   loc = SURFstrongest(point).Location;
   SWS = SURFstrongest(point).Metric / 250;%2e3 originally
   SWS = round(SWS);
   y = loc(1);
   x = loc(2);
   for i=1:m
      for j=1:n
         if(dist([i,j],[x,y]) < SWS)
             d(i,j) = d(i,j) + 10;
         end
      end
   end
end

dd = imgaussfilt(d,1);
ddd = uint8(dd*100);


% fusedIm = imfuse(im,ddd);
% subplot(1,1,1);
% imshow(fusedIm);

[connComp,numBeacons] = bwlabel(ddd);
Sdata = regionprops(connComp,'BoundingBox');
for i=1:numBeacons
   b = imcrop(im,Sdata(i).BoundingBox);
   subplot(1,numBeacons,i);
   imshow(b);
end

% for beacon_idx = 1:numBeacons
%     b = find(connComp==beacon_idx);
% end

function out = dist(p1,p2)
    x = p1(1)-p2(1);
    y = p1(2)-p2(2);
    
    out = sqrt(x^2 + y^2);
end