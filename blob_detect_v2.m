clear; clc;

% im = double(imread('letter_shots/2.png'));
im = imread('../letter_shots/2.png');
% im = imresize(im,0.5);
im = double(im);
[m,n,d] = size(im);

X = reshape(im,size(im,1)*size(im,2),3);
coeff = pca(X);
Itransformed = X*coeff;
final = zeros(m,n,3);
stacked = zeros(m,n,1);

final(:,:,1) = (reshape(Itransformed(:,1),m,n));
final(:,:,2) = (reshape(Itransformed(:,2),m,n));
final(:,:,3) = (reshape(Itransformed(:,3),m,n));

for i=1:3
   data = final(:,:,i);
   final(:,:,i) = abs(final(:,:,i) - mean(mean(data)));
   stacked = stacked + (final(:,:,i) / mean(mean(final(:,:,i))));
end

CAMERA_FOV = 90;
ELEVATION = 300;%height in feet
MIN_FEATURE_SIZE_PX = int32(1080 * (2*atand(ELEVATION/0.5)/CAMERA_FOV));%1500 is a good baseline
PRE_BLUR = 1;
SHRINK_FACTOR = 1.0;

stack_small = imresize(stacked,SHRINK_FACTOR);
stack_small = stack_small .^2;
otsu_thresh = graythresh(stack_small);
stack_small_thresh = imbinarize(uint8(stack_small),otsu_thresh);

[connComp,numTargets] = bwlabel(stack_small_thresh);
connComp = bwareaopen(connComp,double(500));%remove the smallest detected ones
Sdata = regionprops(connComp,'BoundingBox','Area','PixelList');
[connComp,numTargets] = bwlabel(connComp);

for i=1:numTargets
   dimensions = Sdata(i).BoundingBox/SHRINK_FACTOR;
   b = imcrop(uint8(im),dimensions);
   subplot(1,numTargets,i);
   imshow(b);
end


function out = map_to_image(im)
    min_val = min(min(im));
    im = im - min_val + 1;
    max_val = max(max(im));
    im = im * (255/max_val);
    out = uint8(im);
end

